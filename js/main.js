var
	clicked = 0;

$(document).on("ready",function(){
	$('#target').on('click',function(){
		if(clicked % 2 == 0){
			$(this).addClass("animation");
			setTimeout(function(){
				$('#target').removeClass("animation");
			},50);
		}
		clicked++;
		$('#clickCount').text("CLICKED: "+clicked);
	});
});